//
//  Comment.m
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/18/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import "Comment.h"

@implementation Comment

- (id)initWithDictionary:(NSDictionary *)commentDict {
    if (!self) {
        self = [[Comment alloc] init];
    }
    
    self.userName = [commentDict valueForKey:@"userName"];
    self.comment = [commentDict valueForKey:@"comment"];
    
    return self;
}

@end
