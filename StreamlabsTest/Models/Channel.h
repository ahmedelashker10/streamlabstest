//
//  Channel.h
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Channel : NSObject

- (id)initWithDictionary:(NSDictionary *)channelDict;

@property (strong, nonatomic) NSString *name;
@property (nonatomic) int count;
@property (nonatomic) BOOL featured;
@property (strong, nonatomic) NSString *thumb;

@end

NS_ASSUME_NONNULL_END
