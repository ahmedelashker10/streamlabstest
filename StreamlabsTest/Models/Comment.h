//
//  Comment.h
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/18/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Comment : NSObject

- (id)initWithDictionary:(NSDictionary *)commentDict;

@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *comment;

@end

NS_ASSUME_NONNULL_END
