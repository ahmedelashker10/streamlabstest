//
//  HomeUserCell.h
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeUserCell : UICollectionViewCell

- (void)makeImagesRound;

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property (weak, nonatomic) IBOutlet UILabel *lblUser;

@property (weak, nonatomic) IBOutlet UIImageView *imgCount;

@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end

NS_ASSUME_NONNULL_END
