//
//  ViewController.m
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import "ViewController.h"
#import "HomeUserCell.h"
#import "CommentCell.h"
#import "Channel.h"
#import "Comment.h"
#import <SDWebImage/SDWebImage.h>
#import <BFPaperButton/BFPaperButton.h>

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *channels;
@property (strong, nonatomic) NSArray *comments;
@property (strong, nonatomic) BFPaperButton *btnRipple;

@property (weak, nonatomic) IBOutlet UITableView *tableComments;
@property (weak, nonatomic) IBOutlet UIView *viewLike;
@property (weak, nonatomic) IBOutlet UILabel *lblLikes;

@property (nonatomic) int likes;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _likes = 0;
    
    [self addRippleButton];
    [self loadJSON];
}

- (void)addRippleButton {
    // Code for adding the ripple button had to be moved here to avoid an IB Designable error  which kept showing in interface builder whenever it tried to process BFPaperButton, which in turn also kept preventing the entire storyboard from rendering... Creepy!
    _btnRipple = [[BFPaperButton alloc] initWithFrame:CGRectMake(0,
                                                                 0,
                                                                 _viewLike.frame.size.width, _viewLike.frame.size.height)];
    [_btnRipple setShadowColor:[UIColor clearColor]];
    [_btnRipple setBackgroundFadeColor:[UIColor clearColor]];
    [_btnRipple setBackgroundColor:[UIColor clearColor]];
    [_btnRipple setRippleFromTapLocation:NO];
    [_btnRipple setRippleBeyondBounds:YES];
    [_btnRipple setTapCircleColor:[UIColor colorWithRed:250.0f/255.0f
                                                  green:174.0f/255.0f
                                                   blue:99.0f/255.0f
                                                  alpha:1.0f]];
    [_btnRipple addTarget:self action:@selector(animateLike:) forControlEvents:UIControlEventTouchUpInside];

    [_viewLike addSubview:_btnRipple];
}

- (void)loadJSON {
    // Loads dummy data from in-bundle JSON files
    _channels = [self loadCollectionFromJSON:@"channels" collectionKey:@"channels"];
    _comments = [self loadCollectionFromJSON:@"comments" collectionKey:@"comments"];
}

- (NSArray*)loadCollectionFromJSON:(NSString *)file collectionKey:(NSString *)key {
    NSString *fileName = [[NSBundle mainBundle] pathForResource:file
                                                         ofType:@"json"];
    
    NSArray *collection;
    if (fileName) {
        NSData *data = [[NSData alloc] initWithContentsOfFile:fileName];
        
        NSError *error;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:&error];
        collection = [dict objectForKey:key];
        
        if (error) {
            NSLog(@"Something went wrong! %@", error.localizedDescription);
        }
        else {
            NSLog(@"Info: %@", collection);
        }
    }
    else {
        NSLog(@"Couldn't find file!");
    }
    return collection;
}

#pragma mark - UICollectionView methods
- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSDictionary *channelDict = [_channels objectAtIndex:indexPath.row];
    Channel *channel = [[Channel alloc] initWithDictionary:channelDict];
    HomeUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeUserCell" forIndexPath:indexPath];
    [cell.imgUser sd_setImageWithURL:[NSURL URLWithString:channel.thumb]];
    [cell.lblUser setText:channel.name];
    if (channel.featured) {
        [cell.imgCount setBackgroundColor:[UIColor whiteColor]];
        [cell.imgCount setImage:[UIImage imageNamed:@"star.png"]];
    }
    else {
        if (channel.count == 0) {
            [cell.imgCount setHidden:YES];
        }
        else {
            [cell.lblCount setHidden:NO];
            [cell.lblCount setText:[NSString stringWithFormat:@"%d", channel.count]];
        }
    }
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _channels.count;
}

#pragma mark - UITableView methods
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSDictionary *commentDict = [_comments objectAtIndex:indexPath.row];
    Comment *comment = [[Comment alloc] initWithDictionary:commentDict];
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    [cell.lblUser setText:comment.userName];
    [cell.lblComment setText:comment.comment];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _comments.count;
}

#pragma mark - IB Actions
- (IBAction)animateLike:(id)sender {
    _likes++;
    
    [_lblLikes setText:[NSString stringWithFormat:@"%d", _likes]];
    [self setRippleDiameter];
    [self startBalloonAnimation];
}

- (void)setRippleDiameter {
    // Doubling the diameter with each new like until it reaches a certain max value
    if (self.likes < 12) {
        [_btnRipple setTapCircleDiameter:_btnRipple.tapCircleDiameter*2];
    }
}

- (void)startBalloonAnimation {
    // Getting a random point between across the launching view's width as the balloon label doesn't start from the same x point
    int randomX = rand() % (30);
    UILabel *lblBalloon = [[UILabel alloc] initWithFrame:
                           CGRectMake(_viewLike.frame.origin.x + randomX,
                                      _viewLike.frame.origin.y,
                                      0,
                                      0)];
    
    // Setting new text based on likes count with new, random font styles
    [lblBalloon setText:[NSString stringWithFormat:@"+%d", _likes]];
    [lblBalloon setTextColor:[UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0]];
    if (self.likes < 11) {
        [lblBalloon setFont:[UIFont boldSystemFontOfSize:18 + _likes]];
    }
    else {
        [lblBalloon setFont:[UIFont boldSystemFontOfSize:28]];
    }
    
    // Stretching the balloon label, launching it and rightfully disposing of it when it's done... Yaaay!
    [lblBalloon sizeToFit];
    [self.view addSubview:lblBalloon];
    [UIView animateWithDuration:1
                     animations:^{
                         lblBalloon.frame
                         = CGRectMake(lblBalloon.frame.origin.x,
                                      20,
                                      lblBalloon.frame.size.width,
                                      lblBalloon.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [lblBalloon removeFromSuperview];
                     }];
}
@end
