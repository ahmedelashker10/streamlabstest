//
//  HomeUserCell.m
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import "HomeUserCell.h"

@implementation HomeUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self makeImagesRound];
}

- (void)makeImagesRound {
    [_imgUser.layer setMasksToBounds:YES];
    
    _imgUser.layer.cornerRadius = _imgUser.frame.size.height / 2;
    _imgCount.layer.cornerRadius = _imgCount.frame.size.height / 2;
}
@end
