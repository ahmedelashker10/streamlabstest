//
//  AppDelegate.h
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

