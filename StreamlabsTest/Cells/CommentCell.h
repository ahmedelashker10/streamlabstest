//
//  CommentCell.h
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/18/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@end

NS_ASSUME_NONNULL_END
