//
//  CommentCell.m
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/18/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
