//
//  Channel.m
//  StreamlabsTest
//
//  Created by Ahmed Elashker on 5/17/19.
//  Copyright © 2019 Ahmed Elashker. All rights reserved.
//

#import "Channel.h"

@implementation Channel

- (id)initWithDictionary:(NSDictionary *)channelDict {
    if (!self) {
        self = [[Channel alloc] init];
    }
    
    self.name = [channelDict valueForKey:@"name"];
    self.count = [[channelDict valueForKey:@"count"] intValue];
    self.thumb = [channelDict valueForKey:@"thumb"];
    
    long featured = [[channelDict objectForKey:@"featured"] longValue];
    if (featured != 0) {
        self.featured = YES;
    }
    
    return self;
}

@end
